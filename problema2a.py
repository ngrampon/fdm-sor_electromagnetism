# SOR(sucessive over-relaxation) method implementation
# Numerical solution for potential and electrical field 
# for a cable with dieletric separating a massive interior
# and a exterior conducting shell
# Reference from the method as in 
# https://www.mathworks.com/matlabcentral/fileexchange/42773-electric-field-of-a-parallel-plate-capacitor-using-2d-poisson-equation
# By: Natália Rampon (00262502)

import matplotlib.pyplot as plt
import numpy as np
import math as m

np.set_printoptions(threshold=np.nan)

loop=1
Nx=100   #Tamanho grid em altura
Ny=100  #Tamanho grid largura
M=4000  #Numero maximo da iteração

#Dados do problema
InteriorRadius = 1
ExteriorRadius = 3
RelativeEpsilonDieletric = 4    #Permissividade relativa do dielétrico
VacuumPermittivity = 8.85418782*10**(-12)
Epsilon = RelativeEpsilonDieletric*VacuumPermittivity
CableLength = 1000
PotentialInterior = 5

#Dados necessarios para método númerico porque a vida não é perfeita meu irmão

centerX = Nx//2
centerY = Ny//2
NumberPointsWithinCable = m.floor((0.41)*Nx);

#Basically a constant matching real life measurements to mesh points
#God, I should document things in only one language
#Mais c'est la vie
#Ref: Page 3 of http://www.ece.utah.edu/~ece6340/LECTURES/Feb1/Nagel%202012%20-%20Solving%20the%20Generalized%20Poisson%20Equation%20using%20FDM.pdf
h = ExteriorRadius/(NumberPointsWithinCable-1)

print("constante h^2 :"+str(h**2))

#Radius in mesh points
MeshExtRad = m.floor(ExteriorRadius/h);
MeshIntRad = m.floor(InteriorRadius/h);
print("raio externo em pontos da mesh: "+str(MeshExtRad))
print("raio interno em pontos da mesh: "+str(MeshIntRad))
#MeshExtRad = ExteriorRadius*10;
#MeshIntRad = InteriorRadius*10;

#Matriz potencial com condições de fronteira
V = np.zeros((Nx, Ny)) #y = x, x = y

#Matriz de cargas definidas por rho
Rho = np.zeros((Nx,Ny))

#Condicoes de fronteira
V[0,:]= 0
V[Nx-1,:]=0
V[:,0]=0
V[:,Ny-1]=0

#Calcular rho
for i in range(Nx):
    for j in range(Ny):
        #Se está na região entre os aneis
        pointRadius = (i-centerX)**2 + (j-centerY)**2
        if( pointRadius < MeshExtRad**2 and pointRadius > MeshIntRad**2):
            Rho[i,j] = 4/(np.sqrt(pointRadius))

#Potencial do anel interno
for i in range(centerX-MeshIntRad,centerX+MeshIntRad+1):
    for j in range(centerY-MeshIntRad,centerY+MeshIntRad+1):
        pointRadius = (i-centerX)**2 + (j-centerY)**2
        if( pointRadius <= MeshIntRad**2):
            V[i,j] = PotentialInterior

Ncount = 0
while loop == 1:
    Rmin = 0
    for i in range (1, Nx-2):
        for j in range(1, Ny-2):
            pointRadius = (i-centerX)**2 + (j-centerY)**2
            if( pointRadius <= MeshIntRad**2 or pointRadius >= MeshExtRad**2):
                    continue;
            if( pointRadius < MeshExtRad**2 and pointRadius > MeshIntRad**2):
                Residue = 0.25*(V[i-1,j]+V[i+1,j]+V[i,j-1]+V[i,j+1]+(Rho[i,j]*h**2)/Epsilon)-V[i,j];
                Rmin = Rmin + abs(Residue)
                V[i,j]= V[i,j] + Residue
    Rmin=Rmin/(Nx*Ny) # Residuo médio
    Ncount = Ncount + 1
    if(Rmin<=10000):
        if(Ncount>M):
            loop=0
            print("Nao converge.")
        else:
            loop=0
            print("Converge em " + str (Ncount))

#Mapa de calor da tensão
plt.imshow(V, cmap='RdPu', interpolation='nearest')
plt.show()

#Linhas do campo elétrico

#Grid 0,10 são escala; 100j é o tamanho da grid
y, x = np.mgrid[0:10:100j, 0:10:100j]

dy, dx = np.gradient(-V)
fig, ax = plt.subplots()


#Magnitude do campo elétrico (não consegui plotar direito)
#EE = np.sqrt(dx**2 + dy**2)

color = -np.log(np.sqrt(dx**2 + dy**2))
#Tem um erro de divisao por zero no log nesse passo da cor, mas ele fica de boas? Então acho que tanto faz

#ax.streamplot(x, y, dx, dy, color=color, linewidth=1, cmap=plt.cm.hot,
#             density=1, arrowstyle='->', arrowsize=1.0)
ax.quiver(x, y, dx, dy, V, cmap=plt.cm.spring)

ax.set(aspect=1, title='Campo Eletrico')
plt.show()
