# SOR(sucessive over-relaxation) method implementation
# Numerical solution for potential and electrical field 
# for a parallel plates capacitor (longitude slice)
# Reference from the method as in 
# https://www.mathworks.com/matlabcentral/fileexchange/42773-electric-field-of-a-parallel-plate-capacitor-using-2d-poisson-equation
# By: Natália Rampon (00262502)

import matplotlib.pyplot as plt
import numpy as np
import math as m

np.set_printoptions(threshold=np.nan)

loop=1
Nx=100   #Tamanho grid em altura
Ny=100  #Tamanho grid largura
M=4000  #Numero maximo da iteração

TamanhoPlaca=80
CoordPlacax=10
centerX = Nx//2
centerY = Ny//2
NumberPointsWithinCable = m.floor((0.41)*Nx);

#Dados do problema
InteriorRadius = 1
ExteriorRadius = 3
RelativeEpsilonDieletric = 4    #Permissividade relativa do dielétrico
VacuumPermittivity = 8.85418782*10**(-12)
Epsilon = RelativeEpsilonDieletric*VacuumPermittivity
PotentialInterior = 5
PotentialExterior = 0
MeshExtRad = ExteriorRadius*10;
MeshIntRad = InteriorRadius*10;

#Basically a constant matching real life measurements to mesh points
#God, I should document things in only one language
#Mais c'est la vie
#Ref: Page 3 of http://www.ece.utah.edu/~ece6340/LECTURES/Feb1/Nagel%202012%20-%20Solving%20the%20Generalized%20Poisson%20Equation%20using%20FDM.pdf
h = ExteriorRadius/(NumberPointsWithinCable-1)

MeshExtRad = m.floor(ExteriorRadius/h);
MeshIntRad = m.floor(InteriorRadius/h);
print(MeshExtRad)
print(MeshIntRad)

#Matriz potencial com condições de fronteira
V = np.zeros((Nx, Ny)) #y = x, x = y

#Matriz de cargas definidas por rho
Rho = np.zeros((Nx,Ny))

#Potencial nas placas
V[MeshExtRad+centerY, CoordPlacax:(CoordPlacax+TamanhoPlaca)] = PotentialExterior
V[centerY-MeshIntRad:centerY+MeshIntRad, CoordPlacax:(CoordPlacax+TamanhoPlaca)] = PotentialInterior
V[centerY-MeshExtRad, CoordPlacax:(CoordPlacax+TamanhoPlaca)] = PotentialExterior

#Cargas no dielétrico
for j in range(centerY-MeshExtRad+1,centerY-MeshIntRad):
        Rho[j,CoordPlacax:(CoordPlacax+TamanhoPlaca)] = 4/abs(j-centerY)
for j in range(centerY+MeshIntRad+1,centerY+MeshExtRad-1):
        Rho[j,CoordPlacax:(CoordPlacax+TamanhoPlaca)] = 4/abs(j-centerY)

#Condicoes de fronteira
V[0,:]= 0
V[Nx-1,:]=0
V[:,0]=0
V[:,Ny-1]=0


Ncount = 0
while loop == 1:
    Rmin = 0
    for i in range (1, Nx-2):
        for j in range(1, Ny-2):
            if((i == MeshExtRad+centerY) or (i == centerY-MeshExtRad) 
                or (i<= MeshIntRad+centerY-1 and i>=centerY-MeshIntRad)):
                if(j >= CoordPlacax) and (j <= CoordPlacax+TamanhoPlaca-1):
                    continue;
            Residue = 0.25*(V[i-1,j]+V[i+1,j]+V[i,j-1]+V[i,j+1]+(Rho[i,j]*h**2)/Epsilon)-V[i,j];
            Rmin = Rmin + abs(Residue)
            V[i,j]= V[i,j] + Residue
    Rmin=Rmin/(Nx*Ny) # Residuo médio

    Ncount = Ncount + 1
    
    if(Rmin<=0.001):
        if(Ncount>M):
            loop=0
            print("Nao converge.")
        else:
            loop=0
            print("Converge em " + str (Ncount))

#Mapa de calor da tensão
plt.title("Potencial")
plt.imshow(V, cmap='RdPu', interpolation='none')
plt.show()

#Linhas do campo elétrico
y, x = np.mgrid[0:5:100j, 0:10:100j]
dy, dx = np.gradient(V)
fig, ax = plt.subplots()


ax.quiver(x, y, dx, dy, V, cmap=plt.cm.spring)

#Magnitude do campo elétrico (não consegui plotar direito)
#EE = np.sqrt(dx**2 + dy**2)

color = -np.log(np.sqrt(dx**2 + dy**2))
#Tem um erro de divisao por zero no log nesse passo da cor, mas ele fica de boas? Então acho que tanto faz

#ax.streamplot(x, y, dx, dy, color=color, linewidth=1, cmap=plt.cm.hot,
#              density=1, arrowstyle='->', arrowsize=1.0)

ax.set(aspect=1.2, title='Campo Eletrico')
plt.show()
