# SOR(sucessive over-relaxation) method implementation
# Numerical solution for potential and electrical field 
# for a parallel plates capacitor
# Reference from the method as in 
# https://www.mathworks.com/matlabcentral/fileexchange/42773-electric-field-of-a-parallel-plate-capacitor-using-2d-poisson-equation
# By: Natália Rampon (00262502)

import matplotlib.pyplot as plt
import numpy as np
import math

np.set_printoptions(threshold=np.nan)

loop=1
Nx=100   #Tamanho grid em altura
Ny=100  #Tamanho grid largura
M=4000  #Numero maximo da iteração

#Dados do problema
CoordPlaca1y=40
CoordPlaca2y=80
CoordPlacax=10
TamanhoPlaca=80
PotencialPlaca1=10
PotencialPlaca2=-10

#Matriz potencial
V = np.zeros((Nx, Ny)) #y = x, x = y

#Descomentar para placas infinitas
#Também precisa mudar o range dos for para só iterar entre as placas, e tirar os if da placa
#V[CoordPlaca1y, :] = PotenciaPlaca1 #tensao na placa superior
#V[CoordPlaca2y, :] = PotenciaPlaca2 #tensao na placa inferior

V[CoordPlaca1y, CoordPlacax:(CoordPlacax+TamanhoPlaca)] = PotencialPlaca1 #tensao na placa superior
V[CoordPlaca2y, CoordPlacax:(CoordPlacax+TamanhoPlaca)] = PotencialPlaca2 #tensao na placa inferior

#Condicoes de fronteira
V[0,:]= 0
V[Nx-1,:]=0
V[:,0]=0
V[:,Ny-1]=0

#Não uso porque fica dando 2 e isso iria piorar a velocidade da convergência
#Mas tá aí
w = math.cos(math.pi/Nx)+math.cos(math.pi/Ny)
w = (8 - np.sqrt(64 - 16*w*w))/(w*w);

Ncount = 0
while loop == 1:
    Rmin = 0
    for i in range (1, Nx-2):
        for j in range(1, Ny-2):
            if(i == CoordPlaca1y) or (i == CoordPlaca2y):
                if(j >= CoordPlacax) and (j <= CoordPlacax+TamanhoPlaca-1):
                    continue;
            Residue = 0.25*(V[i-1,j]+V[i+1,j]+V[i,j-1]+V[i,j+1])-V[i,j];
            Rmin = Rmin + abs(Residue)
            V[i,j]= V[i,j] + Residue
    Rmin=Rmin/(Nx*Ny) # Residuo médio

    Ncount = Ncount + 1
    
    if(Rmin<=0.001):
        if(Ncount>M):
            loop=0
            print("Nao converge.")
        else:
            loop=0
            print("Converge em " + str (Ncount))

#Mapa de calor da tensão
plt.title("Potencial")
plt.imshow(V, cmap='RdPu', interpolation='nearest')
plt.show()

#Linhas do campo elétrico
y, x = np.mgrid[0:10:100j, 0:10:100j]
dy, dx = np.gradient(-V)
fig, ax = plt.subplots()

#O mapa de quiver fica meio feio
skip = (slice(None, None, 3), slice(None, None, 3))
#ax.quiver(x[skip], y[skip], dx[skip], dy[skip], V[skip],angles='xy', scale_units='xy', scale=1, cmap=plt.cm.RdPu)


#Magnitude do campo elétrico (não consegui plotar direito)
#EE = np.sqrt(dx**2 + dy**2)

color = np.log(np.sqrt(dx**2 + dy**2))
#Tem um erro de divisao por zero no log nesse passo da cor, mas ele fica de boas? Então acho que tanto faz

ax.streamplot(x, y, dx, dy, color=color, linewidth=1, cmap=plt.cm.RdPu,
              density=1, arrowstyle='->', arrowsize=1.0)

ax.set(aspect=1, title='Campo Eletrico')
plt.show()
