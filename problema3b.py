# SOR(sucessive over-relaxation) method implementation
# Numerical solution for potential and electrical field 
# for a comb-drive
# Reference from the method as in 
# https://www.mathworks.com/matlabcentral/fileexchange/42773-electric-field-of-a-parallel-plate-capacitor-using-2d-poisson-equation
# By: Natália Rampon (00262502)
import matplotlib.pyplot as plt
import numpy as np

# Função para ver se um dado ponto está dentro do pente
def pointInCombs(i,j):
    return  (i in range(2*EspacoEntreComb,3*EspacoEntreComb) and j in range (2*EspacoEntreComb,(LarguraComb+2)*EspacoEntreComb)) or \
            (i in range(2*EspacoEntreComb,(AlturaComb+2)*EspacoEntreComb) and j in range (4*EspacoEntreComb,5*EspacoEntreComb)) or \
            (i in range(2*EspacoEntreComb,(AlturaComb+2)*EspacoEntreComb) and j in range (8*EspacoEntreComb,9*EspacoEntreComb)) or \
            (i in range(2*EspacoEntreComb,(AlturaComb+2)*EspacoEntreComb) and j in range (12*EspacoEntreComb,13*EspacoEntreComb)) or \
            (i in range(11*EspacoEntreComb,12*EspacoEntreComb) and j in range (2*EspacoEntreComb+EspacoEntreComb//2,(LarguraComb+1)*EspacoEntreComb+EspacoEntreComb//2)) or \
            (i in range(4*EspacoEntreComb,(AlturaComb+4)*EspacoEntreComb) and j in range (2*EspacoEntreComb+EspacoEntreComb//2,3*EspacoEntreComb+EspacoEntreComb//2)) or \
            (i in range(4*EspacoEntreComb,(AlturaComb+4)*EspacoEntreComb) and j in range (6*EspacoEntreComb+EspacoEntreComb//2,7*EspacoEntreComb+EspacoEntreComb//2)) or \
            (i in range(4*EspacoEntreComb,(AlturaComb+4)*EspacoEntreComb) and j in range (10*EspacoEntreComb+EspacoEntreComb//2,11*EspacoEntreComb+EspacoEntreComb//2))

#Dados do problema

PotencialSuperior = 5
PotencialInferior = -5
LarguraComb = 12
AlturaComb = 8
EspacoEntreComb = 10

loop=1

Nx=(LarguraComb+4)*EspacoEntreComb
Ny=(LarguraComb+4)*EspacoEntreComb

M=4000

#Matriz potencial
V = np.zeros((Nx, Ny)) #y = x, x = y

# Comb Superior
V[2*EspacoEntreComb:3*EspacoEntreComb,2*EspacoEntreComb:(LarguraComb+2)*EspacoEntreComb] = PotencialSuperior #barra superior
V[2*EspacoEntreComb:(AlturaComb+2)*EspacoEntreComb, 4*EspacoEntreComb:5*EspacoEntreComb] = PotencialSuperior  #coluna da esquerda
V[2*EspacoEntreComb:(AlturaComb+2)*EspacoEntreComb, 8*EspacoEntreComb:9*EspacoEntreComb] = PotencialSuperior  #coluna do centro
V[2*EspacoEntreComb:(AlturaComb+2)*EspacoEntreComb, 12*EspacoEntreComb:13*EspacoEntreComb] = PotencialSuperior  #coluna da direita

#Comb Inferior
V[11*EspacoEntreComb:12*EspacoEntreComb,2*EspacoEntreComb+EspacoEntreComb//2:(LarguraComb+1)*EspacoEntreComb+EspacoEntreComb//2] = PotencialInferior #barra inferior
V[4*EspacoEntreComb:(AlturaComb+4)*EspacoEntreComb, 2*EspacoEntreComb+EspacoEntreComb//2:3*EspacoEntreComb+EspacoEntreComb//2] = PotencialInferior  #coluna da esquerda
V[4*EspacoEntreComb:(AlturaComb+4)*EspacoEntreComb, 6*EspacoEntreComb+EspacoEntreComb//2:7*EspacoEntreComb+EspacoEntreComb//2] = PotencialInferior  #coluna do centro
V[4*EspacoEntreComb:(AlturaComb+4)*EspacoEntreComb, 10*EspacoEntreComb+EspacoEntreComb//2:11*EspacoEntreComb+EspacoEntreComb//2] = PotencialInferior  #coluna da direita

#Condicoes de fronteira
V[0,:]= 0
V[Nx-1,:]=0
V[:,0]=0
V[:,Ny-1]=0


Ncount = 0
while loop == 1:
    Rmin = 0
    for i in range (1, Nx-2):
        for j in range(1, Ny-2):
            if(pointInCombs(i,j)):
                continue
            Residue = 0.25*(V[i-1,j]+V[i+1,j]+V[i,j-1]+V[i,j+1])-V[i,j]
            Rmin = Rmin + abs(Residue)
            V[i,j]= V[i,j] + Residue
            
    Rmin=Rmin/(Nx*Ny) # Residuo médio
    
    if(Rmin <= 0.001):
        if(Ncount > M):
            loop=0
            print("nao converge.")
        else:
            loop=0
            print ("Converge em " + str (Ncount))

plt.title("Potencial")
plt.imshow(V, cmap='RdPu', interpolation='nearest')
plt.show()

y, x = np.mgrid[16:0:160j, 16:0:160j]
dy, dx = np.gradient(V)
fig, ax = plt.subplots()
skip = (slice(None, None, 3), slice(None, None, 3))
ax.quiver(x[skip], y[skip], dx[skip], dy[skip], V[skip], cmap=plt.cm.spring)

ax.set(aspect=1, title='Campo Eletrico')
plt.show()
